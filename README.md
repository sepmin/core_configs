# Simple Web Configuration File
This file is used in week 2 lab.
The file shipped with `lu16d-coremu-v1.3` has an issue causing DNS problem for the VM itself.
The `simple-web-fixed.imn` file resolves this issue.